package com.giorgoch.documentmanagementsystem.model;

import jakarta.persistence.*;

import java.time.LocalDateTime;


@Entity
@SequenceGenerator(name = "doc_seq", sequenceName = "document_sequence", allocationSize = 1)
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "doc_seq")
    private Long id;

    private String documentName;

    private String hash;

    private String previousHash;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime timestamp;

    @ManyToOne
    @JoinColumn(name = "document_metadata_id")
    private DocumentMetadata documentMetadata;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public DocumentMetadata getDocumentMetadata() {
        return documentMetadata;
    }

    public void setDocumentMetadata(DocumentMetadata documentMetadata) {
        this.documentMetadata = documentMetadata;
    }

    public Document() {
    }
     public Long getId() {
         return id;
     }

     @PrePersist
     protected void onCreate() {
         timestamp = LocalDateTime.now();
     }

     public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public void setApproved(boolean b) {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

