package com.giorgoch.documentmanagementsystem.model;

public class UploadResponse {
    private int statusCode;
    private String fileUrl;
    private String s3Key;
    private String documentName;
    private String message;
    public UploadResponse(int statusCode, String fileUrl, String s3Key, String documentName, String message) {
        this.statusCode = statusCode;
        this.fileUrl = fileUrl;
        this.s3Key = s3Key;
        this.documentName = documentName;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getS3Key() {
        return s3Key;
    }

    public void setS3Key(String s3Key) {
        this.s3Key = s3Key;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}