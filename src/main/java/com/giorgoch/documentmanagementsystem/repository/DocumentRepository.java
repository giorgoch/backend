package com.giorgoch.documentmanagementsystem.repository;

import com.giorgoch.documentmanagementsystem.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {
    Document findTopByOrderByIdDesc();
}
