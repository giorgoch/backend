package com.giorgoch.documentmanagementsystem.repository;


import com.giorgoch.documentmanagementsystem.model.DocumentMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentMetadataRepository extends JpaRepository<DocumentMetadata, Long> {

}
