package com.giorgoch.documentmanagementsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DocumentmanagementsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocumentmanagementsystemApplication.class, args);
	}

}
