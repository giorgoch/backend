package com.giorgoch.documentmanagementsystem.config;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetBucketLocationRequest;

import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.S3Exception;


import java.lang.invoke.MethodHandles;
import java.net.URI;

@Configuration
@Profile("local")
public class LocalStackConfig {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${aws.region}")
    private String awsRegion;

    @Value("${aws.access-key-id}")
    private String accessKeyId;

    @Value("${aws.secret-access-key}")
    private String secretAccessKey;

    @Value("${aws.bucketName}")
    private String bucket_name;

    @Value("${aws.endpoint.s3}")
    private String aws_Endpoint;

    @PostConstruct
    public void init() {
        logger.info("region :" + awsRegion);
        logger.info("accessKeyId " + accessKeyId);
        logger.info("secretAccessKey " + secretAccessKey);
        logger.info("bucket name " + bucket_name);


    }

    private void connectionVerify(S3Client s3Client) {
        try {
            s3Client.getBucketLocation(GetBucketLocationRequest.builder().bucket(bucket_name).build());
            logger.info("Connected to S3 bucket: {}", bucket_name);
        } catch (NoSuchBucketException e) {
            logger.error("Bucket does not exist: {}", bucket_name);
        } catch (S3Exception e) {
            logger.error("S3 error occurred while connecting to bucket: {}", bucket_name, e);
        } catch (Exception e) {
            logger.error("Failed to connect to S3 bucket: {}", bucket_name, e);
        }
    }

    @Bean
    @Lazy
    public S3Client s3Client() {
        S3Client s3Client = S3Client.builder()
                .endpointOverride(URI.create(aws_Endpoint))
                .region(Region.of(awsRegion))
                .credentialsProvider(StaticCredentialsProvider.create(
                        AwsBasicCredentials.create(accessKeyId, secretAccessKey)
                ))
                .forcePathStyle(true)
                .build();
        connectionVerify(s3Client);
        return s3Client;
    }
}