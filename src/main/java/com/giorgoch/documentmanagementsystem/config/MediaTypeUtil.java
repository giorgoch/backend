package com.giorgoch.documentmanagementsystem.config;

import org.springframework.http.MediaType;

public class MediaTypeUtil {

    public static MediaType getMediaType(String mimeType) {
        switch (mimeType) {
            case "application/pdf":
                return MediaType.APPLICATION_PDF;
            case "image/jpeg":
                return MediaType.IMAGE_JPEG;
            case "image/png":
                return MediaType.IMAGE_PNG;

            default:
                return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
}
