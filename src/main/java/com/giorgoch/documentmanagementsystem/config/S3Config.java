package com.giorgoch.documentmanagementsystem.config;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;

import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.S3ClientBuilder;
import software.amazon.awssdk.services.s3.model.HeadBucketRequest;


import java.lang.invoke.MethodHandles;


@Configuration
@Profile("default")
public class S3Config {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Value("${aws.region}")
    private String awsRegion;

    @Value("${aws.access-key-id}")
    private String accessKeyId;

    @Value("${aws.secret-access-key}")
    private String secretAccessKey;

    @Value("${aws.bucketName}")
    private String bucket_name;

    @PostConstruct
    public void init() {
        logger.info("region :" + awsRegion);
        logger.info("accessKeyId " + accessKeyId);
        logger.info("secretAccessKey " + secretAccessKey);
        logger.info("bucket name " + bucket_name);
    }

    @Bean
    public S3Client s3Client() {
        S3ClientBuilder builder = S3Client.builder()
                .region(Region.of(awsRegion))
                .credentialsProvider(StaticCredentialsProvider.create(
                        AwsBasicCredentials.create(accessKeyId, secretAccessKey)));
        S3Client client = builder.build();
        connectionVerify(client);
        return builder.build();
    }

    private void connectionVerify(S3Client s3Client) {

        try {
            s3Client.headBucket(HeadBucketRequest.builder().bucket(bucket_name).build());
            logger.info("Connected to S3 bucket: {}", bucket_name);
        } catch (Exception e) {
            logger.error("Failed to connect to S3 bucket: {}", bucket_name, e);
        }
    }

}