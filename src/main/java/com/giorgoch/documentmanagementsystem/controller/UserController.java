package com.giorgoch.documentmanagementsystem.controller;

import com.giorgoch.documentmanagementsystem.model.User;
import com.giorgoch.documentmanagementsystem.model.UserResponse;
import com.giorgoch.documentmanagementsystem.service.DocumentService;
import com.giorgoch.documentmanagementsystem.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.invoke.MethodHandles;
import java.util.List;

@RestController
@RequestMapping("/api/users/")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private final DocumentService documentService;
    @Autowired
    private final UserService userService;

    @Autowired
    public UserController(DocumentService documentService, UserService userService) {
        this.documentService = documentService;
        this.userService = userService;
    }

    @PostMapping("/createUserFolder")
    public ResponseEntity createUserFolder(@RequestParam("userName") String userName    ) {
        try {
            userService.createFolderForUser(userName);
            return new ResponseEntity<>("Folder created successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to create folder.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/createuser")
    public ResponseEntity<UserResponse> createUser(@RequestBody User user   ) {
        try {
            UserResponse response = userService.createUser(user);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(new UserResponse(null, null, null, null, e.getMessage()), HttpStatus.CONFLICT);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new UserResponse(null, null, null, null, "Failed to create user."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable Long id) {
        try {
            userService.deleteUser(id);
            return new ResponseEntity<>("User deleted successfully.", HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Failed to delete user.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/edit/{id}")
    public ResponseEntity<UserResponse> editUser(@PathVariable Long id, @RequestBody User user) {
        try {
            UserResponse response = userService.editUser(id, user);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(new UserResponse(null, null, null, null, e.getMessage()), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new UserResponse(null, null, null, null, "Failed to update user."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/deleteUserFolder")
    public ResponseEntity<String> deleteUserFolder(
            @RequestParam String organizationId,
            @RequestParam String userId) {
        try {
            userService.deleteUserFolder(organizationId, userId);
            return new ResponseEntity<>("User folder and all contents deleted successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to delete user folder.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUser(@PathVariable Long id) {
        try {
            UserResponse userResponse = userService.getUserById(id);
            return new ResponseEntity<>(userResponse, HttpStatus.OK);
        } catch (RuntimeException e) {
            return new ResponseEntity<>(new UserResponse(null, null, null, null, e.getMessage()), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new UserResponse(null, null, null, null, "Failed to retrieve user."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/all")
    public ResponseEntity<List<UserResponse>> getAllUsers() {
        try {
            List<UserResponse> users = userService.getAllUsers();
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}