package com.giorgoch.documentmanagementsystem.controller;


import com.giorgoch.documentmanagementsystem.model.UploadResponse;
import com.giorgoch.documentmanagementsystem.service.DocumentService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/document/")
public class DocumentController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    private final DocumentService documentService;
    @Value("${aws.region}")
    private String awsRegion;

    @Value("${aws.access-key-id}")
    private String accessKeyId;

    @Value("${aws.secret-access-key}")
    private String secretAccessKey;

    @Value("${aws.bucketName}")
    private String bucket_name;

    @Autowired
    private final S3Client s3Client;

    @Autowired
    public DocumentController(DocumentService documentService,S3Client s3Client) {

        this.s3Client = s3Client;
        this.documentService = documentService;
    }


    @PostMapping("/upload/{userId}")
    public ResponseEntity<UploadResponse> uploadDocument(@RequestParam("file") MultipartFile file,@PathVariable Long userId) {
        try {
            UploadResponse response = documentService.uploadDocument(file , userId);
            return new ResponseEntity<>(response, HttpStatus.valueOf(response.getStatusCode()));
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/list-files/")
    public ResponseEntity<List<String>> listFiles() {
        try {
            ListObjectsV2Request listObjectsRequest = ListObjectsV2Request.builder()
                    .bucket(bucket_name)
                    .build();

            ListObjectsV2Response response = s3Client.listObjectsV2(listObjectsRequest);
            List<String> fileNames = response.contents().stream()
                    .map(S3Object::key)
                    .collect(Collectors.toList());

            return new ResponseEntity<>(fileNames, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/list-files/{userId}")
    public ResponseEntity<List<Map<String, String>>>listFilesForUser(@PathVariable String userId) {
        try {
            String prefix = "users/" + userId + "/";
            ListObjectsV2Request listObjectsRequest = ListObjectsV2Request.builder()
                    .bucket(bucket_name)
                    .prefix(prefix)
                    .build();

            ListObjectsV2Response response = s3Client.listObjectsV2(listObjectsRequest);
            List<Map<String, String>> files = response.contents().stream()
                    .map(s3Object -> {
                        Map<String, String> fileData = new HashMap<>();
                        fileData.put("filePath", s3Object.key());
                        fileData.put("fileName", s3Object.key().substring(s3Object.key().lastIndexOf('/') + 1));
                        return fileData;
                    })
                    .filter(fileData -> !fileData.get("filePath").endsWith("/"))
                    .collect(Collectors.toList());

            return new ResponseEntity<>(files, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/download/{s3Key}/{id}")
    public ResponseEntity<byte[]> downloadDocument(@PathVariable String s3Key,Long id) {
        try (InputStream inputStream = documentService.downloadDocument(s3Key,id)) {
            byte[] content = inputStream.readAllBytes();
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + s3Key);
            return new ResponseEntity<>(content, headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/deletedocument/{userId}/{filename}")
    public ResponseEntity<String> deleteDocument(
             @PathVariable("userId") String userId,
            @PathVariable("filename") String fileName) {
        logger.info("Start delete for userId: {}, fileName: {}", userId, fileName);
        try {
            documentService.deleteDocument(userId, fileName);
            return  ResponseEntity.ok("Document deleted successfully.");
        } catch (Exception e) {
            logger.error("Failed to delete document: {}", e.getMessage(), e);
            return new ResponseEntity<>("Failed to delete document.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> getDocument(@PathVariable Long id) {
        try {
            Resource file = documentService.getDocument(id);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        } catch (Exception e) {
            logger.error("Failed to retrieve document", e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}