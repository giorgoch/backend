package com.giorgoch.documentmanagementsystem.service;

import com.giorgoch.documentmanagementsystem.model.User;
import com.giorgoch.documentmanagementsystem.model.UserResponse;
import com.giorgoch.documentmanagementsystem.repository.DocumentMetadataRepository;
import com.giorgoch.documentmanagementsystem.repository.DocumentRepository;
import com.giorgoch.documentmanagementsystem.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.lang.invoke.MethodHandles;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {


    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    private S3Client s3Client;
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private DocumentMetadataRepository metadataRepository;
    @Autowired
    private UserRepository userRepository;
    @Value("${aws.bucketName}")
    private  String bucketName;


    public void createFolderForUser( String userId) {
        String folderKey = bucketName + "/" + userId + "/";
        try {
            PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                    .bucket(bucketName)
                    .key(folderKey)
                    .build();


            s3Client.putObject(putObjectRequest, RequestBody.fromBytes(new byte[0]));
        } catch (S3Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error creating folder in S3", e);
        }
    }


    public void deleteUserFolder(String organizationId, String userId) {
        String folderKey = organizationId + "/" + userId + "/";

        try {

            ListObjectsV2Request listObjectsV2Request = ListObjectsV2Request.builder()
                    .bucket(bucketName)
                    .prefix(folderKey)
                    .build();

            ListObjectsV2Response listObjectsV2Response;
            do {
                listObjectsV2Response = s3Client.listObjectsV2(listObjectsV2Request);

                for (S3Object s3Object : listObjectsV2Response.contents()) {
                    s3Client.deleteObject(DeleteObjectRequest.builder()
                            .bucket(bucketName)
                            .key(s3Object.key())
                            .build());
                }
                listObjectsV2Request = listObjectsV2Request.toBuilder()
                        .continuationToken(listObjectsV2Response.nextContinuationToken())
                        .build();
            } while (listObjectsV2Response.isTruncated());

            s3Client.deleteObject(DeleteObjectRequest.builder()
                    .bucket(bucketName)
                    .key(folderKey)
                    .build());

        } catch (S3Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error deleting user folder from S3", e);
        }
    }
    public UserResponse createUser(User user) {

        if (userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new RuntimeException("User with this email already exists.");
        }
        user.setActive(true);
        user.setCreatedDate(LocalDateTime.now());
        User savedUser = userRepository.save(user);
        createFolderInS3(user.getId());
        return new UserResponse(
                savedUser.getId(),
                savedUser.getName(),
                savedUser.getLastName(),
                savedUser.getEmail(),
                "User created successfully."
        );
    }

    public void deleteUser(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found."));
        userRepository.delete(user);
    }

    public UserResponse editUser(Long id, User user) {
        User existingUser = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found."));
        existingUser.setName(user.getName());
        existingUser.setLastName(user.getLastName());
        existingUser.setEmail(user.getEmail());
        existingUser.setAddress(user.getAddress());

        User updatedUser = userRepository.save(existingUser);

        return new UserResponse(
                updatedUser.getId(),
                updatedUser.getName(),
                updatedUser.getLastName(),
                updatedUser.getEmail(),
                "User updated successfully."
        );
    }
    private void createFolderInS3(Long userId) {
        String folderName = "users/" + userId + "/";
        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(folderName)
                .contentType("application/x-directory")
                .build();

        s3Client.putObject(putObjectRequest, RequestBody.empty());
    }

    public UserResponse getUserById(Long id) {
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            return new UserResponse(user.getId(), user.getName(), user.getEmail(), user.getLastName(), null);
        } else {
            throw new RuntimeException("User not found");
        }
    }

    public List<UserResponse> getAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(user -> new UserResponse(user.getId(), user.getName(), user.getLastName(),user.getEmail(),  null))
                .collect(Collectors.toList());
    }
}
