package com.giorgoch.documentmanagementsystem.service;

import com.giorgoch.documentmanagementsystem.config.HashUtil;
import com.giorgoch.documentmanagementsystem.config.MediaTypeUtil;
import com.giorgoch.documentmanagementsystem.model.Document;
import com.giorgoch.documentmanagementsystem.model.DocumentMetadata;
import com.giorgoch.documentmanagementsystem.model.UploadResponse;
import com.giorgoch.documentmanagementsystem.model.User;
import com.giorgoch.documentmanagementsystem.repository.DocumentMetadataRepository;
import com.giorgoch.documentmanagementsystem.repository.DocumentRepository;
import com.giorgoch.documentmanagementsystem.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.sync.RequestBody;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    private S3Client s3Client;
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private DocumentMetadataRepository metadataRepository;
    @Autowired
    private UserRepository userRepository;
    private  String bucketName;
    @Autowired
    public DocumentService(S3Client s3Client, @Value("${aws.bucketName}") String bucketName) {
        this.s3Client = s3Client;
        this.bucketName = bucketName;
    }

    public MediaType getMediaType(String mimeType) {
        switch (mimeType) {
            case "application/pdf":
                return MediaType.APPLICATION_PDF;
            case "image/jpeg":
                return MediaType.IMAGE_JPEG;
            case "image/png":
                return MediaType.IMAGE_PNG;

            default:
                return MediaType.APPLICATION_OCTET_STREAM;
        }
    }

    public UploadResponse uploadDocument(MultipartFile file ,Long userId) throws IOException {
        logger.info("starting the upload");
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User not found"));

        String bucketFolder =null;
        Document lastDocument = documentRepository.findTopByOrderByIdDesc();
        logger.info("Starting the process for upload to s3 send sns  etc..");
        String filename = file.getOriginalFilename();
        String fileExtension = "";
        if (filename != null && filename.contains(".")) {
            fileExtension = filename.substring(filename.lastIndexOf(".") + 1);
        }
        if (userId !=null){
            bucketFolder = String.format("users/%d/", userId);
        }
        logger.info("File extension: " + fileExtension);
        String mimeType = fileExtension ;

        String s3Key = bucketFolder + filename;
        String uniqueFileName = UUID.randomUUID().toString();

        logger.info(s3Key + "to path");
        MediaType mediaType = MediaTypeUtil.getMediaType(mimeType);


        try (InputStream fileInputStream = file.getInputStream()) {
            PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                    .bucket(bucketName)
                    .key(s3Key)
                    .contentType(mediaType.toString())
                    .build();

            s3Client.putObject(putObjectRequest, RequestBody.fromInputStream(fileInputStream, file.getSize()));

            String fileUrl = String.format("https://%s.s3.amazonaws.com/%s", bucketName, s3Key);
            logger.info("success uploading file adding metadata to database  ");
            DocumentMetadata metadata = new DocumentMetadata();
            metadata.setFilename(file.getOriginalFilename());
            metadata.setS3Key(s3Key);
            metadata.setFilename(filename);
            metadata.setSize(file.getSize());

            metadataRepository.save(metadata);
            String previousHash = lastDocument != null ? lastDocument.getHash() : "0";
            String currentHash = HashUtil.generateHash(new String("") + previousHash);

            Document doc = new Document();
            doc.setDocumentMetadata(metadata);
            doc.setDocumentName(filename);
            doc.setApproved(true);
            doc.setUser(user);
            doc.setHash(currentHash);
            doc.setPreviousHash(previousHash);
            doc.setTimestamp(LocalDateTime.now());


            documentRepository.save(doc);
            logger.info(" metadata added to database returning response code");

            return new UploadResponse(HttpStatus.OK.value(), fileUrl, s3Key,"","");
        } catch (S3Exception | NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("Error uploading file to S3", e);
        }
    }

    public InputStream downloadDocument(String s3Key,Long id ) {
        logger.info("donwload document");
        try {
            GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(s3Key)
                    .build();

            return s3Client.getObject(getObjectRequest);
        } catch (S3Exception e) {

            logger.info("Error getting object from S3: " + e.awsErrorDetails().errorMessage());
            e.printStackTrace();
            throw new RuntimeException("Error downloading file from S3", e);
        }
    }

    public UploadResponse editDocument(Long id, MultipartFile file) throws IOException {
        Document doc = documentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Document not found"));

        String oldS3Key = doc.getDocumentMetadata().getS3Key();
        s3Client.deleteObject(DeleteObjectRequest.builder()
                .bucket(bucketName)
                .key(oldS3Key)
                .build());

        return uploadDocument(file, doc.getUser().getId());
    }

    public Resource getDocument(Long id) {
        logger.info("get document");
        Document doc = documentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Document not found"));

        String s3Key = doc.getDocumentMetadata().getS3Key();

        GetObjectRequest getObjectRequest = GetObjectRequest.builder()
                .bucket(bucketName)
                .key(s3Key)
                .build();


        ResponseInputStream<GetObjectResponse> responseStream = s3Client.getObject(getObjectRequest);


        return new InputStreamResource(responseStream);
    }


    public void deleteDocument(String userId, String fileName) {
        logger.info("Service delete started for userId: {}, fileName: {}", userId, fileName);

        String fileKey = String.format("users/%s/%s", userId, fileName);
        logger.info("Constructed fileKey for deletion: {}", fileKey);

        try {

            DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder()
                    .bucket(bucketName)
                    .key(fileKey)
                    .build();


            s3Client.deleteObject(deleteObjectRequest);
            logger.info("Delete operation invoked for: {}", fileKey);


            try {
                s3Client.headObject(HeadObjectRequest.builder()
                        .bucket(bucketName)
                        .key(fileKey)
                        .build());
                logger.error("File still exists after delete: {}", fileKey);
            } catch (NoSuchKeyException e) {
                logger.info("File successfully deleted: {}", fileKey);
            }
        } catch (S3Exception e) {
            logger.error("S3Exception during delete: {}", e.awsErrorDetails().errorMessage(), e);
            throw new RuntimeException("Error deleting file from S3", e);
        } catch (Exception e) {
            logger.error("General exception during delete: {}", e.getMessage(), e);
            throw new RuntimeException("General error deleting file", e);
        }
    }




}