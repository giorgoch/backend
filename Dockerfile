
FROM openjdk:17-jdk-slim

WORKDIR /app

COPY target/documentmanagementsystem-0.0.1-snapshot.jar documentmanagementsystem-0.0.1-snapshot.jar


ENTRYPOINT ["java", "-jar", "documentmanagementsystem-0.0.1-snapshot.jar"]

EXPOSE 8080